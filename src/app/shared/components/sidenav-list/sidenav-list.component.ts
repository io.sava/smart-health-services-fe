import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { User } from '../../models/users/user.model';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
    @Output() sidenavClose = new EventEmitter();

    constructor(private authenticationService: AuthenticationService) { }

    ngOnInit(): void {}

    public onSidenavClose = () => {
        this.sidenavClose.emit();
    }

    public logout(): void {
        this.authenticationService.logout();
    }

    public getUser(): User {
        return this.authenticationService.currentUserValue;
    }

    public isUserLoggedIn(): boolean {
        return Object.keys(this.getUser()).length !== 0;
    }

    public isUserAdmin(): boolean {
        return this.getUser().role === 'ADMIN';
    }

    public isUserDoctor(): boolean {
        return this.getUser().role === 'DOCTOR';
    }

    public isUserEmployee(): boolean {
        return this.getUser().role !== 'PATIENT';
    }
}
