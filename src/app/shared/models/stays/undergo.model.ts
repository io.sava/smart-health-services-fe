import { Doctor } from '../persons/doctor.model';
import { Procedure } from './procedure.model';

export interface Undergo {
    id: number;
    date: Date;
    procedure: Procedure;
    doctor: Doctor;
}
