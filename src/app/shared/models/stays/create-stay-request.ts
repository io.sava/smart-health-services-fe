export interface CreateStayRequest {
    startDate: string;
    endDate: string;
    roomId: number;
    patientId: number;
}
