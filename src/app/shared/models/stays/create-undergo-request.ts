export interface CreateUndergoRequest {
    stayId: number;
    doctorId: number;
    procedureId: number;
}
