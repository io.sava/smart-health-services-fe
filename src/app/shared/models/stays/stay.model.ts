import { Room } from '../hospitals/room.model';
import { Patient } from '../persons/patient.model';

export interface Stay {
    id: number;
    startDate: Date;
    endDate: Date;
    room: Room;
    patient: Patient;
}
