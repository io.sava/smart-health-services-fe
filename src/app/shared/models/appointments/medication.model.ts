export interface Medication {
    name: string;
    brand: string;
}
