import { Appointment } from './appointment.model';
import { Medication } from './medication.model';

export interface Prescription {
    dose: string;
    medication: Medication;
    appointment: Appointment;
}
