import { Doctor } from '../persons/doctor.model';
import { Nurse } from '../persons/nurse.model';
import { Patient } from '../persons/patient.model';

export interface Appointment {
    id: number;
    diagnosis: string;
    date: Date;
    doctor: Doctor;
    nurse: Nurse;
    patient: Patient;
}
