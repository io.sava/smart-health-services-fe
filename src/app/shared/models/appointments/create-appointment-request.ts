export interface CreateAppointmentRequest {
    date: string;
    diagnosis: string;
    patientId: number;
    nurseId: number;
    doctorId: number;
}
