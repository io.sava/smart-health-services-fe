export interface CreatePrescriptionRequest {
    dose: string;
    appointmentId: number;
    medicationId: number;
}
