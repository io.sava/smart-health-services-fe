export interface Hospital {
    id: number;
    name: string;
    address: string;
    fax: string;
    email: string;
    phone: string;
}
