import { Hospital } from './hospital.model';

export interface Department {
    id: number;
    name: string;
    hospital: Hospital;
}
