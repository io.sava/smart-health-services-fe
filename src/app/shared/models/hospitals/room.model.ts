import { Hospital } from './hospital.model';

export interface Room {
    id: number;
    type: string;
    floor: number;
    hospital: Hospital;
}
