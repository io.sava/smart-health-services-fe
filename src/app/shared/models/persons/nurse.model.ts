import { Department } from '../hospitals/department';
import { Employee } from './employee.model';

export class Nurse implements Employee {
    id!: number;
    name!: string;
    email!: string;
    position!: string;
    department!: Department;
}
