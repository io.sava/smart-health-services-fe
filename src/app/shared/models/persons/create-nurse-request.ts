export interface CreateNurseRequest {
    name: string;
    email: string;
    position: string;
    departmentId: number;
}
