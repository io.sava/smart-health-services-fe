import { Department } from '../hospitals/department';
import { Employee } from './employee.model';

export class Doctor implements Employee {
    id!: number;
    name!: string;
    email!: string;
    position!: string;
    department!: Department;
}
