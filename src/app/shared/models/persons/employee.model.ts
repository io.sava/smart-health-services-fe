import { Department } from '../hospitals/department';
import { Person } from './person.model';

export class Employee implements Person {
    id!: number;
    name!: string;
    email!: string;
    department!: Department;
}
