export interface CreateDoctorRequest {
    name: string;
    email: string;
    position: string;
    departmentId: number;
}
