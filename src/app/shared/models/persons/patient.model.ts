import { Person } from './person.model';

export class Patient implements Person {
    id!: number;
    cnp!: string;
    name!: string;
    age!: number;
    address!: string;
    phone!: string;
    email!: string;
}
