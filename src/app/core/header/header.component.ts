import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from 'src/app/shared/models/users/user.model';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    title = 'Servicii de Sănătate Inteligentă';
    @Output() public sidenavToggle = new EventEmitter();

    constructor(private authenticationService: AuthenticationService) { }

    ngOnInit(): void { }

    public onToggleSidenav = () => {
        this.sidenavToggle.emit();
    }

    public logout(): void {
        this.authenticationService.logout();
    }

    public getUser(): User {
        return this.authenticationService.currentUserValue;
    }

    public isUserLoggedIn(): boolean {
        return Object.keys(this.getUser()).length !== 0;
    }

    public isUserAdmin(): boolean {
        return this.getUser().role === 'ADMIN';
    }

    public isUserDoctor(): boolean {
        return this.getUser().role === 'DOCTOR';
    }

    public isUserEmployee(): boolean {
        return this.getUser().role !== 'PATIENT';
    }
}
