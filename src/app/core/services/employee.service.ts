import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Doctor } from 'src/app/shared/models/persons/doctor.model';
import { Nurse } from 'src/app/shared/models/persons/nurse.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
    constructor(private http: HttpClient) { }

    getById(id: number, role: string): Observable<any> {
        if (role === 'DOCTOR') {
            return this.http.get<Doctor>(`${environment.apiUrl}/api/v1/doctors/${id}`);
        } else if (role === 'NURSE') {
            return this.http.get<Nurse>(`${environment.apiUrl}/api/v1/nurses/${id}`);
        }
        return of({});
    }
}

