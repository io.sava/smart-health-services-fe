import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Department } from 'src/app/shared/models/hospitals/department';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
    constructor(private http: HttpClient) { }

    getById(id: number): Observable<Department> {
        return this.http.get<Department>(`${environment.apiUrl}/api/v1/departments/${id}`);
    }

    getByHospitalId(hospitalId: number): Observable<Department[]> {
        return this.http.get<Department[]>(`${environment.apiUrl}/api/v1/departments/hospitals/${hospitalId}`);
    }
}
