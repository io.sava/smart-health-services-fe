import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Patient } from 'src/app/shared/models/persons/patient.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
    constructor(private http: HttpClient) { }

    getAll(page: number, size: number): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/api/v1/patients?page=${page}&size=${size}`);
    }

    getById(id: number): Observable<Patient> {
        return this.http.get<Patient>(`${environment.apiUrl}/api/v1/patients/${id}`);
    }

    create(patient: Patient): Observable<any> {
        return this.http.post<Patient>(`${environment.apiUrl}/api/v1/patients`, patient);
    }
}
