import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Medication } from 'src/app/shared/models/appointments/medication.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedicationService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<Medication[]> {
        return this.http.get<Medication[]>(`${environment.apiUrl}/api/v1/medications`);
    }
}
