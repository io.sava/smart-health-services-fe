import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateNurseRequest } from 'src/app/shared/models/persons/create-nurse-request';
import { Nurse } from 'src/app/shared/models/persons/nurse.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NurseService {
    constructor(private http: HttpClient) { }

    getAll(page: number, size: number): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/api/v1/nurses?page=${page}&size=${size}`);
    }

    getById(id: number): Observable<Nurse> {
        return this.http.get<Nurse>(`${environment.apiUrl}/api/v1/nurses/${id}`);
    }

    getByDepartmentId(departmentId: number): Observable<Nurse[]> {
        return this.http.get<Nurse[]>(`${environment.apiUrl}/api/v1/nurses/departments/${departmentId}`);
    }

    create(nurse: CreateNurseRequest): Observable<any> {
        return this.http.post<CreateNurseRequest>(`${environment.apiUrl}/api/v1/nurses`, nurse);
    }
}
