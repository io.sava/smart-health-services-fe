import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Procedure } from 'src/app/shared/models/stays/procedure.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProcedureService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<Procedure[]> {
        return this.http.get<Procedure[]>(`${environment.apiUrl}/api/v1/procedures`);
    }
}
