import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from 'src/app/shared/models/hospitals/room.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
    constructor(private http: HttpClient) { }

    getByHospitalId(hospitalId: number): Observable<Room[]> {
        return this.http.get<Room[]>(`${environment.apiUrl}/api/v1/rooms/hospitals/${hospitalId}`);
    }
}
