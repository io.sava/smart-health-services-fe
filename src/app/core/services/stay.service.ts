import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateStayRequest } from 'src/app/shared/models/stays/create-stay-request';
import { Stay } from 'src/app/shared/models/stays/stay.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StayService {
    constructor(private http: HttpClient) { }

    getByPatientId(patientId: number): Observable<Stay[]> {
        return this.http.get<Stay[]>(`${environment.apiUrl}/api/v1/stays/patients/${patientId}`);
    }

    getById(id: number): Observable<Stay> {
        return this.http.get<Stay>(`${environment.apiUrl}/api/v1/stays/${id}`);
    }

    create(stay: CreateStayRequest): Observable<any> {
        return this.http.post<CreateStayRequest>(`${environment.apiUrl}/api/v1/stays`, stay);
    }

    update(id: number, stay: CreateStayRequest): Observable<any> {
        return this.http.put<CreateStayRequest>(`${environment.apiUrl}/api/v1/stays/${id}`, stay);
    }

    delete(stayId: number): Observable<any> {
        return this.http.delete(`${environment.apiUrl}/api/v1/stays/${stayId}`);
    }
}
