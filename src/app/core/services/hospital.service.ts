import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Hospital } from 'src/app/shared/models/hospitals/hospital.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HospitalService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<Hospital[]> {
        return this.http.get<Hospital[]>(`${environment.apiUrl}/api/v1/hospitals`);
    }

    getById(id: number): Observable<Hospital> {
        return this.http.get<Hospital>(`${environment.apiUrl}/api/v1/hospitals/${id}`);
    }
}
