import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Appointment } from 'src/app/shared/models/appointments/appointment.model';
import { CreateAppointmentRequest } from 'src/app/shared/models/appointments/create-appointment-request';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
    constructor(private http: HttpClient) { }

    getByPatientId(patientId: number): Observable<Appointment[]> {
        return this.http.get<Appointment[]>(`${environment.apiUrl}/api/v1/appointments/patients/${patientId}`);
    }

    getByDoctorId(doctorId: number): Observable<Appointment[]> {
        return this.http.get<Appointment[]>(`${environment.apiUrl}/api/v1/appointments/doctors/${doctorId}`);
    }

    getByNurseId(nurseId: number): Observable<Appointment[]> {
        return this.http.get<Appointment[]>(`${environment.apiUrl}/api/v1/appointments/nurses/${nurseId}`);
    }

    getById(id: number): Observable<Appointment> {
        return this.http.get<Appointment>(`${environment.apiUrl}/api/v1/appointments/${id}`);
    }

    exportPrescriptions(id: number): Observable<Blob> {
        return this.http.get(`${environment.apiUrl}/api/v1/appointments/${id}/prescriptions/export`, {responseType: 'blob'});
    }

    create(appointment: CreateAppointmentRequest): Observable<any> {
        return this.http.post<CreateAppointmentRequest>(`${environment.apiUrl}/api/v1/appointments`, appointment);
    }

    update(id: number, appointment: CreateAppointmentRequest): Observable<any> {
        return this.http.put<CreateAppointmentRequest>(`${environment.apiUrl}/api/v1/appointments/${id}`, appointment);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${environment.apiUrl}/api/v1/appointments/${id}`);
    }
}
