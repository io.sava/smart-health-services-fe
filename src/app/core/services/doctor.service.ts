import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateDoctorRequest } from 'src/app/shared/models/persons/create-doctor-request';
import { Doctor } from 'src/app/shared/models/persons/doctor.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
    constructor(private http: HttpClient) { }

    getAll(page: number, size: number): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/api/v1/doctors?page=${page}&size=${size}`);
    }

    getById(id: number): Observable<Doctor> {
        return this.http.get<Doctor>(`${environment.apiUrl}/api/v1/doctors/${id}`);
    }

    getByDepartmentId(departmentId: number): Observable<Doctor[]> {
        return this.http.get<Doctor[]>(`${environment.apiUrl}/api/v1/doctors/departments/${departmentId}`);
    }

    create(doctor: CreateDoctorRequest): Observable<any> {
        return this.http.post<CreateDoctorRequest>(`${environment.apiUrl}/api/v1/doctors`, doctor);
    }
}
