import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateUndergoRequest } from 'src/app/shared/models/stays/create-undergo-request';
import { Undergo } from 'src/app/shared/models/stays/undergo.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UndergoService {
    constructor(private http: HttpClient) { }

    getByStayId(stayId: number): Observable<Undergo[]> {
        return this.http.get<Undergo[]>(`${environment.apiUrl}/api/v1/undergoes/stays/${stayId}`);
    }

    getByDoctorId(doctorId: number, page: number, size: number): Observable<any> {
        return this.http.get<Undergo[]>(`${environment.apiUrl}/api/v1/undergoes/doctors?doctorId=${doctorId}&page=${page}&size=${size}`);
    }

    create(undergo: CreateUndergoRequest): Observable<any> {
        return this.http.post<CreateUndergoRequest>(`${environment.apiUrl}/api/v1/undergoes`, undergo);
    }

    delete(undergoId: number): Observable<any> {
        return this.http.delete(`${environment.apiUrl}/api/v1/undergoes/${undergoId}`);
    }
}
