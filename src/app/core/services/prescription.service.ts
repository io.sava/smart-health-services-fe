import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreatePrescriptionRequest } from 'src/app/shared/models/appointments/create-prescription-request';
import { Prescription } from 'src/app/shared/models/appointments/prescription.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrescriptionService {
    constructor(private http: HttpClient) { }

    getByAppointmentId(appointmentId: number): Observable<Prescription[]> {
        return this.http.get<Prescription[]>(`${environment.apiUrl}/api/v1/prescriptions/appointments/${appointmentId}`);
    }

    create(prescription: CreatePrescriptionRequest): Observable<any> {
        return this.http.post<CreatePrescriptionRequest>(`${environment.apiUrl}/api/v1/prescriptions`, prescription);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${environment.apiUrl}/api/v1/prescriptions/${id}`);
    }
}
