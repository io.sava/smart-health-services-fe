import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { EmployeeService } from '../services/employee.service';
import { StayService } from '../services/stay.service';

@Injectable({ providedIn: 'root' })
export class UpdateStayGuard implements CanActivate {
    STAY_ID_KEY = 'stayId';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private employeeService: EmployeeService,
        private stayService: StayService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        const currentUser = this.authenticationService.currentUserValue;
        const stayId = +route.params[this.STAY_ID_KEY];

        if (currentUser.role === 'DOCTOR' || currentUser.role === 'NURSE') {
            return forkJoin([
                this.employeeService.getById(currentUser.ownerId as number, currentUser.role as string),
                this.stayService.getById(stayId)]).pipe(map(([employee, stay]) => {
                    if (employee.department.hospital.id === stay.room.hospital.id) {
                        return true;
                    }
                    this.router.navigate(['/']);
                    return false;
                }));
        }

        this.router.navigate(['/']);
        return false;
    }
}
