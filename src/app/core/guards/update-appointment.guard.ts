import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppointmentService } from '../services/appointment.service';
import { AuthenticationService } from '../services/authentication.service';
import { NurseService } from '../services/nurse.service';

@Injectable({ providedIn: 'root' })
export class UpdateAppointmentGuard implements CanActivate {
    APPOINTMENT_ID_KEY = 'appointmentId';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private nurseService: NurseService,
        private appointmentService: AppointmentService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        const currentUser = this.authenticationService.currentUserValue;
        const appointmentId = +route.params[this.APPOINTMENT_ID_KEY];

        if (currentUser.role === 'NURSE') {
            return forkJoin([
                this.nurseService.getById(currentUser.ownerId as number),
                this.appointmentService.getById(appointmentId)]).pipe(map(([nurse, appointment]) => {
                    if (nurse.id === appointment.nurse.id) {
                        return true;
                    }
                    this.router.navigate(['/']);
                    return false;
                }));
        }

        this.router.navigate(['/']);
        return false;
    }
}
