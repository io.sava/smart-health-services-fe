import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppointmentService } from '../services/appointment.service';
import { AuthenticationService } from '../services/authentication.service';
import { EmployeeService } from '../services/employee.service';

@Injectable({ providedIn: 'root' })
export class CreatePrescriptionGuard implements CanActivate {
    APPOINTMENT_ID_KEY = 'appointmentId';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private employeeService: EmployeeService,
        private appointmentService: AppointmentService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        const currentUser = this.authenticationService.currentUserValue;
        const appointmentId = +route.params[this.APPOINTMENT_ID_KEY];

        if (currentUser.role === 'DOCTOR' || currentUser.role === 'NURSE') {
            return forkJoin([
                this.employeeService.getById(currentUser.ownerId as number, currentUser.role as string),
                this.appointmentService.getById(appointmentId)]).pipe(map(([employee, appointment]) => {
                    if (currentUser.role === 'DOCTOR') {
                        return employee.id === appointment.doctor.id;
                    }
                    if (currentUser.role === 'NURSE') {
                        return employee.id === appointment.nurse.id;
                    }
                    this.router.navigate(['/']);
                    return false;
                }));
        }

        this.router.navigate(['/']);
        return false;
    }
}
