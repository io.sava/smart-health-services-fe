import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { DoctorService } from '../services/doctor.service';
import { StayService } from '../services/stay.service';

@Injectable({ providedIn: 'root' })
export class CreateUndergoGuard implements CanActivate {
    STAY_ID_KEY = 'stayId';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private doctorService: DoctorService,
        private stayService: StayService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        const currentUser = this.authenticationService.currentUserValue;

        if (currentUser.role === 'DOCTOR') {
            const stayId = +route.params[this.STAY_ID_KEY];
            return forkJoin([this.doctorService.getById(currentUser.ownerId as number),
                            this.stayService.getById(stayId)]).pipe(map(([doctor, stay]) => {
                                if (doctor.department.hospital.id === stay.room.hospital.id) {
                                    return true;
                                }
                                this.router.navigate(['/']);
                                return false;
                            }));
        }

        this.router.navigate(['/']);
        return false;
    }
}
