import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({ providedIn: 'root' })
export class DoctorDataGuard implements CanActivate {
    ID_KEY = 'id';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser.role === 'DOCTOR' && currentUser.ownerId === +route.params[this.ID_KEY]) {
            return true;
        }

        this.router.navigate(['/']);
        return false;
    }
}
