import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({ providedIn: 'root' })
export class PatientDataGuard implements CanActivate {
    ID_KEY = 'id';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser.role === 'PATIENT' && currentUser.ownerId !== +route.params[this.ID_KEY]) {
            this.router.navigate(['/']);
            return false;
        }

        return true;
    }
}
