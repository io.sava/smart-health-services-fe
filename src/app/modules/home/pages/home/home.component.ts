import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    constructor(
        private titleService: Title,
        private authenticationService: AuthenticationService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.titleService.setTitle('Acasă');
    }

    isUserAuthenticated(): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        return Object.keys(currentUser).length !== 0;
    }

    goToLoginPage(): void {
        this.router.navigate(['autentificare']);
    }
}
