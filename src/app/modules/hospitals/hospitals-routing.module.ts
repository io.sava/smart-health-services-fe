import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentComponent } from './pages/department/department.component';
import { HospitalComponent } from './pages/hospital/hospital.component';
import { HospitalsComponent } from './pages/hospitals/hospitals.component';

const routes: Routes = [
    {
        path: '',
        component: HospitalsComponent
    },
    {
        path: ':id',
        component: HospitalComponent
    },
    {
        path: 'departamente/:departmentId',
        component: DepartmentComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalsRoutingModule { }
