import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';
import { DepartmentService } from 'src/app/core/services/department.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { NurseService } from 'src/app/core/services/nurse.service';
import { Department } from 'src/app/shared/models/hospitals/department';
import { Doctor } from 'src/app/shared/models/persons/doctor.model';
import { Nurse } from 'src/app/shared/models/persons/nurse.model';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit {
    department: Department | undefined;
    doctors$: Observable<Doctor[]> | undefined;
    nurses$: Observable<Nurse[]> | undefined;

    constructor(
        private titleService: Title,
        private route: ActivatedRoute,
        private departmentService: DepartmentService,
        private doctorService: DoctorService,
        private nurseService: NurseService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        this.departmentService.getById(this.getDepartmentId()).subscribe(
            department => {
                this.department = department;
            },
            error => {
                this.alertService.error(error);
            }
        );
        this.doctors$ = this.doctorService.getByDepartmentId(this.getDepartmentId());
        this.nurses$ = this.nurseService.getByDepartmentId(this.getDepartmentId());
        this.titleService.setTitle('Departamente');
    }

    getDepartmentId(): number {
        return this.route.snapshot.params.departmentId;
    }
}
