import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';
import { DepartmentService } from 'src/app/core/services/department.service';
import { HospitalService } from 'src/app/core/services/hospital.service';
import { RoomService } from 'src/app/core/services/room.service';
import { Department } from 'src/app/shared/models/hospitals/department';
import { Hospital } from 'src/app/shared/models/hospitals/hospital.model';
import { Room } from 'src/app/shared/models/hospitals/room.model';

@Component({
  selector: 'app-hospital',
  templateUrl: './hospital.component.html',
  styleUrls: ['./hospital.component.scss']
})
export class HospitalComponent implements OnInit {
    hospital: Hospital | undefined;
    departments$: Observable<Department[]> | undefined;
    rooms$: Observable<Room[]> | undefined;

    constructor(
        private titleService: Title,
        private route: ActivatedRoute,
        private hospitalService: HospitalService,
        private departmentService: DepartmentService,
        private roomService: RoomService,
        private alertService: AlertService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.hospitalService.getById(this.getHospitalId()).subscribe(
            hospital => {
                this.hospital = hospital;
            },
            error => {
                this.alertService.error(error);
            }
        );
        this.departments$ = this.departmentService.getByHospitalId(this.getHospitalId());
        this.rooms$ = this.roomService.getByHospitalId(this.getHospitalId());
        this.titleService.setTitle('Spitale');
    }

    getHospitalId(): number {
        return this.route.snapshot.params.id;
    }

    redirectToDepartmentPage(departmentId: number): void {
        this.router.navigate([`/spitale/departamente/${departmentId}`]);
    }
}
