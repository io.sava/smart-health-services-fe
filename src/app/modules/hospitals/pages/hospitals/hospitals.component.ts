import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HospitalService } from 'src/app/core/services/hospital.service';
import { Hospital } from 'src/app/shared/models/hospitals/hospital.model';

@Component({
  selector: 'app-hospitals',
  templateUrl: './hospitals.component.html',
  styleUrls: ['./hospitals.component.scss']
})
export class HospitalsComponent implements OnInit {
    hospitals$: Observable<Hospital[]> | undefined;

    constructor(
        private titleService: Title,
        private hospitalService: HospitalService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.hospitals$ = this.hospitalService.getAll();
        this.titleService.setTitle('Spitale');
    }

    redirectToHospitalPage(hospitalId: number): void {
        this.router.navigate([`/spitale/${hospitalId}`]);
    }
}
