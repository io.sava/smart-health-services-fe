import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HospitalsRoutingModule } from './hospitals-routing.module';
import { HospitalsComponent } from './pages/hospitals/hospitals.component';
import { HospitalComponent } from './pages/hospital/hospital.component';
import { DepartmentComponent } from './pages/department/department.component';


@NgModule({
  declarations: [HospitalsComponent, HospitalComponent, DepartmentComponent],
  imports: [
    CommonModule,
    HospitalsRoutingModule
  ]
})
export class HospitalsModule { }
