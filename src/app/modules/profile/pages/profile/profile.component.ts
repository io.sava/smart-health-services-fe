import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { NurseService } from 'src/app/core/services/nurse.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { User } from 'src/app/shared/models/users/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    person$: Observable<any> | undefined;
    user: User;

    constructor(
        private titleService: Title,
        private authenticationService: AuthenticationService,
        private patientService: PatientService,
        private doctorService: DoctorService,
        private nurseService: NurseService
    ) {
        this.user = this.authenticationService.currentUserValue;
    }

    ngOnInit(): void {
        this.titleService.setTitle('Profil');
        this.initializePerson();
    }

    private initializePerson(): void {
        if (this.user.role === 'PATIENT') {
            this.person$ = this.patientService.getById(this.user.ownerId as number);
        } else if (this.user.role === 'DOCTOR') {
            this.person$ = this.doctorService.getById(this.user.ownerId as number);
        } else if (this.user.role === 'NURSE') {
            this.person$ = this.nurseService.getById(this.user.ownerId as number);
        }
    }
}
