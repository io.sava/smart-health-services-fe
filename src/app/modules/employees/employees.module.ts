import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutingModule } from './employees-routing.module';
import { CategoryComponent } from './pages/category/category.component';
import { DoctorsComponent } from './pages/doctors/doctors.component';
import { NursesComponent } from './pages/nurses/nurses.component';
import { CreateNurseComponent } from './pages/create-nurse/create-nurse.component';
import { CreateDoctorComponent } from './pages/create-doctor/create-doctor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { DoctorUndergoesComponent } from './pages/doctor-undergoes/doctor-undergoes.component';


@NgModule({
  declarations: [
    CategoryComponent,
    DoctorsComponent,
    NursesComponent,
    CreateNurseComponent,
    CreateDoctorComponent,
    DoctorUndergoesComponent
    ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmployeesRoutingModule,
    CustomMaterialModule
  ]
})
export class EmployeesModule { }
