import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UndergoService } from 'src/app/core/services/undergo.service';
import { Undergo } from 'src/app/shared/models/stays/undergo.model';

@Component({
  selector: 'app-doctor-undergoes',
  templateUrl: './doctor-undergoes.component.html',
  styleUrls: ['./doctor-undergoes.component.scss']
})
export class DoctorUndergoesComponent implements OnInit {
    CONTENT_KEY = 'content';
    TOTAL_ELEMENTS_KEY = 'totalElements';

    totalUndergoes = 0;
    undergoes: Undergo[] = [];

    constructor(
        private titleService: Title,
        private undergoService: UndergoService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.setUndergoes(0, 5);
        this.titleService.setTitle('Operații');
    }

    getDoctorId(): number {
        return this.route.snapshot.params.id;
    }

    setUndergoes(page: number, size: number): void {
        this.undergoService.getByDoctorId(this.getDoctorId(), page, size).subscribe(
            data => {
                this.undergoes = data[this.CONTENT_KEY],
                this.totalUndergoes = data[this.TOTAL_ELEMENTS_KEY];
            }
        );
    }

    deleteUndergo(id: number): void {
        this.undergoService.delete(id).subscribe(
            () => this.setUndergoes(0, 5)
        );
    }

    public nextPage(event: PageEvent): void {
        const page = event.pageIndex;
        const size = event.pageSize;
        this.setUndergoes(page, size);
    }
}
