import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { DepartmentService } from 'src/app/core/services/department.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { HospitalService } from 'src/app/core/services/hospital.service';
import { Department } from 'src/app/shared/models/hospitals/department';
import { Hospital } from 'src/app/shared/models/hospitals/hospital.model';

@Component({
  selector: 'app-create-doctor',
  templateUrl: './create-doctor.component.html',
  styleUrls: ['./create-doctor.component.scss']
})
export class CreateDoctorComponent implements OnInit {
    createDoctorForm = this.formBuilder.group({
        name: new FormControl('', Validators.required),
        hospital: new FormControl('', Validators.required),
        department: new FormControl('', Validators.required),
        position: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email])
    });

    hospitals$: Observable<Hospital[]> | undefined;
    departments$: Observable<Department[]> | undefined;
    departmentId = 0;
    loading = false;

    constructor(
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private doctorService: DoctorService,
        private hospitalService: HospitalService,
        private departmentService: DepartmentService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        this.hospitals$ = this.hospitalService.getAll();
        this.titleService.setTitle('Creare doctor');
    }

    get fields(): any {
        return this.createDoctorForm.controls;
    }

    updateDepartments(hospitalId: number): void {
        this.departments$ = this.departmentService.getByHospitalId(hospitalId);
    }

    updateDepartmentId(departmentId: number): void {
        this.departmentId = departmentId;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.createDoctorForm.invalid) {
            return;
        }

        this.loading = true;
        this.doctorService
            .create({
                name: this.fields.name.value,
                position: this.fields.position.value,
                email: this.fields.email.value,
                departmentId: this.departmentId
            })
            .pipe(first())
            .subscribe(
                () => {
                    this.router.navigate(['/personal/doctori']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }
}
