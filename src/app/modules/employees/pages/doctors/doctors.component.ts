import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { Doctor } from 'src/app/shared/models/persons/doctor.model';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {
    CONTENT_KEY = 'content';
    TOTAL_ELEMENTS_KEY = 'totalElements';

    totalDoctors = 0;
    doctors: Doctor[] = [];

    constructor(
        private titleService: Title,
        private doctorService: DoctorService,
        private authenticationService: AuthenticationService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.setDoctors(0, 5);
        this.titleService.setTitle('Doctori');
    }

    setDoctors(page: number, size: number): void {
        this.doctorService.getAll(page, size).subscribe(
            data => {
                this.doctors = data[this.CONTENT_KEY],
                this.totalDoctors = data[this.TOTAL_ELEMENTS_KEY];
            }
        );
    }

    goToCreateDoctor(): void {
        this.router.navigate(['personal/doctori/creare']);
    }

    public isUserAdmin(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'ADMIN';
    }

    public nextPage(event: PageEvent): void {
        const page = event.pageIndex;
        const size = event.pageSize;
        this.setDoctors(page, size);
    }
}
