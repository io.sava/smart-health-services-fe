import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NurseService } from 'src/app/core/services/nurse.service';
import { Nurse } from 'src/app/shared/models/persons/nurse.model';

@Component({
  selector: 'app-nurses',
  templateUrl: './nurses.component.html',
  styleUrls: ['./nurses.component.scss']
})
export class NursesComponent implements OnInit {
    CONTENT_KEY = 'content';
    TOTAL_ELEMENTS_KEY = 'totalElements';

    totalNurses = 0;
    nurses: Nurse[] = [];

    constructor(
        private titleService: Title,
        private nurseService: NurseService,
        private authenticationService: AuthenticationService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.setNurses(0, 5);
        this.titleService.setTitle('Asistente');
    }

    setNurses(page: number, size: number): void {
        this.nurseService.getAll(page, size).subscribe(
            data => {
                this.nurses = data[this.CONTENT_KEY],
                this.totalNurses = data[this.TOTAL_ELEMENTS_KEY];
            }
        );
    }

    goToCreateNurse(): void {
        this.router.navigate(['personal/asistente/creare']);
    }

    public isUserAdmin(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'ADMIN';
    }

    public nextPage(event: PageEvent): void {
        const page = event.pageIndex;
        const size = event.pageSize;
        this.setNurses(page, size);
    }
}
