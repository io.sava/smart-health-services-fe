import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

    constructor(
        private titleService: Title
    ) { }

    ngOnInit(): void {
        this.titleService.setTitle('Personal');
    }
}
