import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { DepartmentService } from 'src/app/core/services/department.service';
import { HospitalService } from 'src/app/core/services/hospital.service';
import { NurseService } from 'src/app/core/services/nurse.service';
import { Department } from 'src/app/shared/models/hospitals/department';
import { Hospital } from 'src/app/shared/models/hospitals/hospital.model';

@Component({
  selector: 'app-create-nurse',
  templateUrl: './create-nurse.component.html',
  styleUrls: ['./create-nurse.component.scss']
})
export class CreateNurseComponent implements OnInit {
    createNurseForm = this.formBuilder.group({
        name: new FormControl('', Validators.required),
        hospital: new FormControl('', Validators.required),
        department: new FormControl('', Validators.required),
        position: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email])
    });

    hospitals$: Observable<Hospital[]> | undefined;
    departments$: Observable<Department[]> | undefined;
    departmentId = 0;
    loading = false;

    constructor(
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private nurseService: NurseService,
        private hospitalService: HospitalService,
        private departmentService: DepartmentService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        this.hospitals$ = this.hospitalService.getAll();
        this.titleService.setTitle('Creare asistentă');
    }

    get fields(): any {
        return this.createNurseForm.controls;
    }

    updateDepartments(hospitalId: number): void {
        this.departments$ = this.departmentService.getByHospitalId(hospitalId);
    }

    updateDepartmentId(departmentId: number): void {
        this.departmentId = departmentId;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.createNurseForm.invalid) {
            return;
        }

        this.loading = true;
        this.nurseService
            .create({
                name: this.fields.name.value,
                position: this.fields.position.value,
                email: this.fields.email.value,
                departmentId: this.departmentId
            })
            .pipe(first())
            .subscribe(
                () => {
                    this.router.navigate(['/personal/asistente']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }
}
