import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorDataGuard } from 'src/app/core/guards/doctor-data.guard';
import { RoleGuard } from 'src/app/core/guards/role.guard';
import { CategoryComponent } from './pages/category/category.component';
import { CreateDoctorComponent } from './pages/create-doctor/create-doctor.component';
import { CreateNurseComponent } from './pages/create-nurse/create-nurse.component';
import { DoctorUndergoesComponent } from './pages/doctor-undergoes/doctor-undergoes.component';
import { DoctorsComponent } from './pages/doctors/doctors.component';
import { NursesComponent } from './pages/nurses/nurses.component';

const routes: Routes = [
    {
        path: '',
        component: CategoryComponent
    },
    {
        path: 'doctori',
        component: DoctorsComponent
    },
    {
        path: 'doctori/creare',
        component: CreateDoctorComponent,
        canActivate: [RoleGuard],
        data: {
            roles: ['PATIENT', 'NURSE', 'DOCTOR']
        }
    },
    {
        path: 'doctori/:id/operatii',
        component: DoctorUndergoesComponent,
        canActivate: [DoctorDataGuard]
    },
    {
        path: 'asistente',
        component: NursesComponent
    },
    {
        path: 'asistente/creare',
        component: CreateNurseComponent,
        canActivate: [RoleGuard],
        data: {
            roles: ['PATIENT', 'NURSE', 'DOCTOR']
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
