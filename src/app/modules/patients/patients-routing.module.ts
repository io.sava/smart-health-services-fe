import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientDataGuard } from 'src/app/core/guards/patient-data.guard';
import { AppointmentsComponent } from './pages/appointments/appointments.component';
import { CreateStayComponent } from './pages/create-stay/create-stay.component';
import { CreateComponent } from './pages/create/create.component';
import { PatientsComponent } from './pages/patients/patients.component';
import { StayComponent } from './pages/stay/stay.component';
import { StaysComponent } from './pages/stays/stays.component';
import { CreateUndergoComponent } from './pages/create-undergo/create-undergo.component';
import { CreateUndergoGuard } from 'src/app/core/guards/create-undergo.guard';
import { UpdateStayComponent } from './pages/update-stay/update-stay.component';
import { UpdateStayGuard } from 'src/app/core/guards/update-stay.guard';
import { AppointmentComponent } from './pages/appointment/appointment.component';
import { CreateAppointmentComponent } from './pages/create-appointment/create-appointment.component';
import { RoleGuard } from 'src/app/core/guards/role.guard';
import { UpdateAppointmentComponent } from './pages/update-appointment/update-appointment.component';
import { UpdateAppointmentGuard } from 'src/app/core/guards/update-appointment.guard';
import { CreatePrescriptionComponent } from './pages/create-prescription/create-prescription.component';
import { CreatePrescriptionGuard } from 'src/app/core/guards/create-prescription.guard';

const routes: Routes = [
    {
        path: '',
        component: PatientsComponent,
        canActivate: [RoleGuard],
        data: {
            roles: ['PATIENT']
        }
    },
    {
        path: 'creare',
        component: CreateComponent,
        canActivate: [RoleGuard],
        data: {
            roles: ['PATIENT', 'DOCTOR']
        }
    },
    {
        path: ':id/internari',
        component: StaysComponent,
        canActivate: [PatientDataGuard]
    },
    {
        path: ':id/internari/creare',
        component: CreateStayComponent,
        canActivate: [RoleGuard],
        data: {
            roles: ['PATIENT', 'ADMIN']
        }
    },
    {
        path: ':id/internari/:stayId',
        component: StayComponent,
        canActivate: [PatientDataGuard]
    },
    {
        path: ':id/internari/:stayId/operatii/creare',
        component: CreateUndergoComponent,
        canActivate: [CreateUndergoGuard]
    },
    {
        path: ':id/internari/:stayId/actualizare',
        component: UpdateStayComponent,
        canActivate: [UpdateStayGuard]
    },
    {
        path: ':id/programari',
        component: AppointmentsComponent,
        canActivate: [PatientDataGuard]
    },
    {
        path: ':id/programari/creare',
        component: CreateAppointmentComponent,
        canActivate: [RoleGuard],
        data: {
            roles: ['PATIENT', 'DOCTOR', 'ADMIN']
        }
    },
    {
        path: ':id/programari/:appointmentId',
        component: AppointmentComponent,
        canActivate: [PatientDataGuard]
    },
    {
        path: ':id/programari/:appointmentId/actualizare',
        component: UpdateAppointmentComponent,
        canActivate: [UpdateAppointmentGuard],
    },
    {
        path: ':id/programari/:appointmentId/prescriptii/creare',
        component: CreatePrescriptionComponent,
        canActivate: [CreatePrescriptionGuard],
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule { }
