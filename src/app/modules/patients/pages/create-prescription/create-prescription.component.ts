import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { MedicationService } from 'src/app/core/services/medication.service';
import { PrescriptionService } from 'src/app/core/services/prescription.service';
import { Medication } from 'src/app/shared/models/appointments/medication.model';

@Component({
  selector: 'app-create-prescription',
  templateUrl: './create-prescription.component.html',
  styleUrls: ['./create-prescription.component.scss']
})
export class CreatePrescriptionComponent implements OnInit {
    createPrescriptionForm = this.formBuilder.group({
        medication: new FormControl('', Validators.required),
        dose: new FormControl('', Validators.required)
    });

    medications: Medication[] = [];
    medicationId = 0;
    loading = false;

    constructor(
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private medicationService: MedicationService,
        private prescriptionService: PrescriptionService
    ) { }

    ngOnInit(): void {
        this.medicationService.getAll().subscribe(
            medications => this.medications = medications
        );
        this.titleService.setTitle('Adăugare medicament la rețetă');
    }

    get fields(): any {
        return this.createPrescriptionForm.controls;
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    getAppointmentId(): number {
        return this.route.snapshot.params.appointmentId;
    }

    updateMedicationId(medicationId: number): void {
        this.medicationId = medicationId;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.createPrescriptionForm.invalid) {
            return;
        }

        this.loading = true;
        this.prescriptionService.create({
                appointmentId: this.getAppointmentId(),
                medicationId: this.medicationId,
                dose: this.fields.dose.value
            }).pipe(first())
            .subscribe(
                () => {
                    this.router.navigate([`/pacienti/${this.getPatientId()}/programari/${this.getAppointmentId()}`]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }
}
