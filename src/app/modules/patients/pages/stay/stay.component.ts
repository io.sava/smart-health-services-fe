import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/core/services/alert.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { StayService } from 'src/app/core/services/stay.service';
import { UndergoService } from 'src/app/core/services/undergo.service';
import { Doctor } from 'src/app/shared/models/persons/doctor.model';
import { Patient } from 'src/app/shared/models/persons/patient.model';
import { Stay } from 'src/app/shared/models/stays/stay.model';
import { Undergo } from 'src/app/shared/models/stays/undergo.model';

@Component({
  selector: 'app-stay',
  templateUrl: './stay.component.html',
  styleUrls: ['./stay.component.scss']
})
export class StayComponent implements OnInit {
    patient: Patient | undefined;
    stay: Stay | undefined;
    doctor: Doctor | undefined;
    undergoes: Undergo[] = [];

    constructor(
        private titleService: Title,
        private patientService: PatientService,
        private stayService: StayService,
        private undergoService: UndergoService,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private router: Router,
        private authenticationService: AuthenticationService,
        private doctorService: DoctorService
    ) { }

    ngOnInit(): void {
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
            },
            error => {
                this.alertService.error(error);
            }
        );

        this.stayService.getById(this.getStayId()).subscribe(
            stay => {
                this.stay = stay;
                this.undergoService.getByStayId(stay.id).subscribe(
                    undergoes => {
                        this.undergoes = undergoes;
                    }
                );
            },
            error => {
                this.alertService.error(error);
            }
        );

        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser.role === 'DOCTOR') {
            this.doctorService.getById(currentUser.ownerId as number).subscribe(
                doctor => {
                    this.doctor = doctor;
                }
            );
        }

        this.titleService.setTitle('Internări');
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    getStayId(): number {
        return this.route.snapshot.params.stayId;
    }

    goToCreateUndergoPage(): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/internari/${this.getStayId()}/operatii/creare`]);
    }

    canUserCreateUndergo(): boolean {
        const currentUser = this.authenticationService.currentUserValue;

        if (currentUser.role === 'DOCTOR') {
            if (this.doctor) {
                if (this.stay) {
                    if (this.doctor.department.hospital.id === this.stay.room.hospital.id) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
