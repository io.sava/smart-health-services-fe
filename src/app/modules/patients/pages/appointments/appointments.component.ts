import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/core/services/alert.service';
import { AppointmentService } from 'src/app/core/services/appointment.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { Appointment } from 'src/app/shared/models/appointments/appointment.model';
import { Patient } from 'src/app/shared/models/persons/patient.model';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
    appointments: Appointment[] = [];
    patient: Patient | undefined;

    constructor(
        private titleService: Title,
        private appointmentService: AppointmentService,
        private patientService: PatientService,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private authenticationService: AuthenticationService
    ) {}

    ngOnInit(): void {
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
                this.setAppointments();
            },
            error => {
                this.alertService.error(error);
            }
        );
        this.titleService.setTitle('Programări');
    }

    setAppointments(): void {
        this.appointmentService.getByPatientId(this.getPatientId()).subscribe(
            appointments => {
                this.appointments = appointments;
            },
            error => {
                this.alertService.error(error);
            }
        );
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    goToAppointmentPage(appointmentId: number): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/programari/${appointmentId}`]);
    }

    goToCreateAppointmentPage(): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/programari/creare`]);
    }

    goToUpdateAppointmentPage(appointmentId: number): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/programari/${appointmentId}/actualizare`]);
    }

    deleteAppointment(appointmentId: number): void {
        this.appointmentService.delete(appointmentId).subscribe(
            () => this.setAppointments()
        );
    }

    isUserNurse(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'NURSE';
    }

    isUserAdmin(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'ADMIN';
    }

    canUserUpdateAppointment(appointmentNurseId: number): boolean {
        if (this.isUserNurse()) {
            const user = this.authenticationService.currentUserValue;
            return user.ownerId === appointmentNurseId;
        }

        return false;
    }

    canUserDeleteAppointment(appointmentNurseId: number): boolean {
        if (this.isUserAdmin()) {
            return true;
        }

        if (this.isUserNurse()) {
            const user = this.authenticationService.currentUserValue;
            return user.ownerId === appointmentNurseId;
        }

        return false;
    }
}
