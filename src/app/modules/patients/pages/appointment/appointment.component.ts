import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/core/services/alert.service';
import { AppointmentService } from 'src/app/core/services/appointment.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { PrescriptionService } from 'src/app/core/services/prescription.service';
import { Appointment } from 'src/app/shared/models/appointments/appointment.model';
import { Prescription } from 'src/app/shared/models/appointments/prescription.model';
import { Patient } from 'src/app/shared/models/persons/patient.model';
import * as FileSaver from 'file-saver';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit {
    patient: Patient | undefined;
    appointment: Appointment | undefined;
    prescriptions: Prescription[] = [];

    constructor(
        private titleService: Title,
        private patientService: PatientService,
        private appointmentService: AppointmentService,
        private prescriptionService: PrescriptionService,
        private route: ActivatedRoute,
        private router: Router,
        private alertService: AlertService,
        private authenticationService: AuthenticationService
    ) { }

    ngOnInit(): void {
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
            },
            error => {
                this.alertService.error(error);
            }
        );

        this.appointmentService.getById(this.getAppointmentId()).subscribe(
            appointment => {
                this.appointment = appointment;
                this.setPrescriptions(appointment.id);
            },
            error => {
                this.alertService.error(error);
            }
        );

        this.titleService.setTitle('Programări');
    }

    setPrescriptions(appointmentId: number): void {
        this.prescriptionService.getByAppointmentId(appointmentId).subscribe(
            prescriptions => {
                this.prescriptions = prescriptions;
            }
        );
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    getAppointmentId(): number {
        return this.route.snapshot.params.appointmentId;
    }

    downloadPrescriptions(): void {
        this.appointmentService.exportPrescriptions(this.getAppointmentId()).subscribe(
            data => {
                FileSaver.saveAs(data, 'Reteta.pdf');
            }
        );
    }

    goToCreatePrescriptionPage(): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/programari/${this.getAppointmentId()}/prescriptii/creare`]);
    }

    deletePrescription(prescriptionId: number): void {
        this.prescriptionService.delete(prescriptionId).subscribe(
            () => {
                if (this.appointment) {
                    this.setPrescriptions(this.appointment.id);
                }
            }
        );
    }

    isUserNurse(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'NURSE';
    }

    isUserDoctor(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'DOCTOR';
    }

    canUserModifyPrescriptions(appointmentNurseId: number, appointmentDoctorId: number): boolean {
        const user = this.authenticationService.currentUserValue;

        if (this.isUserNurse()) {
            return user.ownerId === appointmentNurseId;
        }

        if (this.isUserDoctor()) {
            return user.ownerId === appointmentDoctorId;
        }

        return false;
    }
}
