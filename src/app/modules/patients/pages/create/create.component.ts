import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { PatientService } from 'src/app/core/services/patient.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
    createPatientForm = this.formBuilder.group({
        name: new FormControl('', Validators.required),
        cnp: new FormControl('', Validators.required),
        age: new FormControl('', Validators.required),
        address: new FormControl(''),
        phone: new FormControl(''),
        email: new FormControl('', [Validators.required, Validators.email])
    });

    loading = false;

    constructor(
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private patientService: PatientService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        this.titleService.setTitle('Creare pacient');
    }

    get fields(): any {
        return this.createPatientForm.controls;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.createPatientForm.invalid) {
            return;
        }

        this.loading = true;
        this.patientService.create({
                id: 0,
                name: this.fields.name.value,
                cnp: this.fields.cnp.value,
                age: this.fields.age.value,
                address: this.fields.address.value,
                phone: this.fields.phone.value,
                email: this.fields.email.value
            }).pipe(first())
            .subscribe(
                () => {
                    this.router.navigate(['/pacienti']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }
}
