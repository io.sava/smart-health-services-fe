import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { NurseService } from 'src/app/core/services/nurse.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { RoomService } from 'src/app/core/services/room.service';
import { StayService } from 'src/app/core/services/stay.service';
import { Room } from 'src/app/shared/models/hospitals/room.model';
import { Patient } from 'src/app/shared/models/persons/patient.model';

@Component({
  selector: 'app-update-stay',
  templateUrl: './update-stay.component.html',
  styleUrls: ['./update-stay.component.scss'],
  providers: [DatePipe]
})
export class UpdateStayComponent implements OnInit {
    START_DATE_KEY = 'startDate';
    END_DATE_KEY = 'endDate';
    ROOM_KEY = 'room';

    updateStayForm = this.formBuilder.group({
        startDate: new FormControl('', Validators.required),
        endDate: new FormControl('', Validators.required),
        room: new FormControl('', Validators.required)
    });

    patient: Patient | undefined;
    rooms: Room[] = [];
    roomId = 0;
    loading = false;

    constructor(
        private datePipe: DatePipe,
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private stayService: StayService,
        private patientService: PatientService,
        private authenticationService: AuthenticationService,
        private doctorService: DoctorService,
        private nurseService: NurseService,
        private roomService: RoomService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        const user = this.authenticationService.currentUserValue;
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
                if (user.role === 'DOCTOR') {
                    this.doctorService.getById(user.ownerId as number).subscribe(
                        doctor => this.roomService.getByHospitalId(doctor.department.hospital.id).subscribe(
                            rooms => this.rooms = rooms
                        )
                    );
                } else {
                    this.nurseService.getById(user.ownerId as number).subscribe(
                        nurse => this.roomService.getByHospitalId(nurse.department.hospital.id).subscribe(
                            rooms => this.rooms = rooms
                        )
                    );
                }
            },
            error => {
                this.alertService.error(error);
            }
        );

        this.stayService.getById(this.getStayId()).subscribe(
            stay => {
                this.updateStayForm.controls[this.START_DATE_KEY].setValue(this.convertDateToFrontEndFormat(stay.startDate));
                this.updateStayForm.controls[this.END_DATE_KEY].setValue(this.convertDateToFrontEndFormat(stay.endDate));
                this.updateStayForm.controls[this.ROOM_KEY].setValue(`${stay.room.type}, etaj ${stay.room.floor}`);
                this.updateRoomId(stay.room.id);
            },
            error => {
                this.alertService.error(error);
            }
        );
        this.titleService.setTitle('Actualizare internare');
    }

    convertDateToFrontEndFormat(date: Date): string {
        return this.datePipe.transform(date, 'yyyy-MM-ddThh:mm') as string;
    }

    get fields(): any {
        return this.updateStayForm.controls;
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    getStayId(): number {
        return this.route.snapshot.params.stayId;
    }

    updateRoomId(roomId: number): void {
        this.roomId = roomId;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.updateStayForm.invalid) {
            return;
        }

        this.loading = true;
        this.stayService.update(this.getStayId(), {
                startDate: this.datePipe.transform(this.fields.startDate.value, 'dd-MM-yyyy HH:mm') as string,
                endDate: this.datePipe.transform(this.fields.endDate.value, 'dd-MM-yyyy HH:mm') as string,
                roomId: this.roomId,
                patientId: this.getPatientId()
            }).pipe(first())
            .subscribe(
                () => {
                    this.router.navigate([`/pacienti/${this.getPatientId()}/internari`]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }
}
