import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { Patient } from 'src/app/shared/models/persons/patient.model';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
    CONTENT_KEY = 'content';
    TOTAL_ELEMENTS_KEY = 'totalElements';

    totalPatients = 0;
    patients: Patient[] = [];

    constructor(
        private titleService: Title,
        private patientService: PatientService,
        private authenticationService: AuthenticationService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.setPatients(0, 5);
        this.titleService.setTitle('Pacienți');
    }

    setPatients(page: number, size: number): void {
        this.patientService.getAll(page, size).subscribe(
            data => {
                this.patients = data[this.CONTENT_KEY],
                this.totalPatients = data[this.TOTAL_ELEMENTS_KEY];
            }
        );
    }

    goToCreatePatient(): void {
        this.router.navigate(['pacienti/creare']);
    }

    public isUserDoctor(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'DOCTOR';
    }

    public nextPage(event: PageEvent): void {
        const page = event.pageIndex;
        const size = event.pageSize;
        this.setPatients(page, size);
    }
}
