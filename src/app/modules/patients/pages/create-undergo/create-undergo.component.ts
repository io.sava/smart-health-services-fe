import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { ProcedureService } from 'src/app/core/services/procedure.service';
import { UndergoService } from 'src/app/core/services/undergo.service';
import { Patient } from 'src/app/shared/models/persons/patient.model';
import { Procedure } from 'src/app/shared/models/stays/procedure.model';

@Component({
  selector: 'app-create-undergo',
  templateUrl: './create-undergo.component.html',
  styleUrls: ['./create-undergo.component.scss']
})
export class CreateUndergoComponent implements OnInit {
    createUndergoForm = this.formBuilder.group({
        procedure: new FormControl('', Validators.required),
    });

    patient: Patient | undefined;
    procedures: Procedure[] = [];
    procedureId = 0;
    loading = false;

    constructor(
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private procedureService: ProcedureService,
        private patientService: PatientService,
        private undergoService: UndergoService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        this.procedureService.getAll().subscribe(
            procedures => this.procedures = procedures
        );
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
            }
        );
        this.titleService.setTitle('Creare operație');
    }

    get fields(): any {
        return this.createUndergoForm.controls;
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    getStayId(): number {
        return this.route.snapshot.params.stayId;
    }

    getDoctorId(): number {
        return this.authenticationService.currentUserValue.ownerId as number;
    }

    updateProcedureId(procedureId: number): void {
        this.procedureId = procedureId;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.createUndergoForm.invalid) {
            return;
        }

        this.loading = true;
        this.undergoService.create({
            doctorId: this.getDoctorId(),
            procedureId: this.procedureId,
            stayId: this.getStayId()
        }).pipe(first())
        .subscribe(
            () => {
                this.router.navigate([`/pacienti/${this.getPatientId()}/internari/${this.getStayId()}`]);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}
