import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { AppointmentService } from 'src/app/core/services/appointment.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { NurseService } from 'src/app/core/services/nurse.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { Doctor } from 'src/app/shared/models/persons/doctor.model';
import { Patient } from 'src/app/shared/models/persons/patient.model';

@Component({
  selector: 'app-update-appointment',
  templateUrl: './update-appointment.component.html',
  styleUrls: ['./update-appointment.component.scss'],
  providers: [DatePipe]
})
export class UpdateAppointmentComponent implements OnInit {
    DATE_KEY = 'date';
    DIAGNOSIS_KEY = 'diagnosis';
    DOCTOR_KEY = 'doctor';

    updateAppointmentForm = this.formBuilder.group({
        date: new FormControl('', Validators.required),
        diagnosis: new FormControl(''),
        doctor: new FormControl('', Validators.required)
    });

    patient: Patient | undefined;
    doctors: Doctor[] = [];
    doctorId = 0;
    loading = false;

    constructor(
        private datePipe: DatePipe,
        private titleService: Title,
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private patientService: PatientService,
        private authenticationService: AuthenticationService,
        private doctorService: DoctorService,
        private alertService: AlertService,
        private appointmentService: AppointmentService,
        private nurseService: NurseService,
    ) { }

    ngOnInit(): void {
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
            },
            error => {
                this.alertService.error(error);
            }
        );

        const user = this.authenticationService.currentUserValue;
        this.nurseService.getById(user.ownerId as number).subscribe(
            nurse => {
                this.doctorService.getByDepartmentId(nurse.department.id).subscribe(
                    doctors => this.doctors = doctors
                );
            }
        );

        this.appointmentService.getById(this.getAppointmentId()).subscribe(
            appointment => {
                this.updateAppointmentForm.controls[this.DATE_KEY].setValue(this.convertDateToFrontEndFormat(appointment.date));
                this.updateAppointmentForm.controls[this.DIAGNOSIS_KEY].setValue(appointment.diagnosis);
                this.updateAppointmentForm.controls[this.DOCTOR_KEY].setValue(appointment.doctor.name);
                this.updateDoctorId(appointment.doctor.id);
            },
            error => {
                this.alertService.error(error);
            }
        );
        this.titleService.setTitle('Creare programare');
    }

    convertDateToFrontEndFormat(date: Date): string {
        return this.datePipe.transform(date, 'yyyy-MM-ddThh:mm') as string;
    }

    get fields(): any {
        return this.updateAppointmentForm.controls;
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    getAppointmentId(): number {
        return this.route.snapshot.params.appointmentId;
    }

    updateDoctorId(doctorId: number): void {
        this.doctorId = doctorId;
    }

    onSubmit(): void {
        this.alertService.clear();

        if (this.updateAppointmentForm.invalid) {
            return;
        }

        this.loading = true;
        this.appointmentService.update(this.getAppointmentId(), {
                date: this.datePipe.transform(this.fields.date.value, 'dd-MM-yyyy HH:mm') as string,
                diagnosis: this.fields.diagnosis.value,
                patientId: this.getPatientId(),
                doctorId: this.doctorId,
                nurseId: this.authenticationService.currentUserValue.ownerId as number
            }).pipe(first())
            .subscribe(
                () => {
                    this.router.navigate([`/pacienti/${this.getPatientId()}/programari`]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }
}
