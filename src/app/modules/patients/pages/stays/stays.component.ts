import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/core/services/alert.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { PatientService } from 'src/app/core/services/patient.service';
import { StayService } from 'src/app/core/services/stay.service';
import { Employee } from 'src/app/shared/models/persons/employee.model';
import { Patient } from 'src/app/shared/models/persons/patient.model';
import { Stay } from 'src/app/shared/models/stays/stay.model';

@Component({
  selector: 'app-stays',
  templateUrl: './stays.component.html',
  styleUrls: ['./stays.component.scss']
})
export class StaysComponent implements OnInit {
    stays: Stay[] = [];
    patient: Patient | undefined;
    employee: Employee | undefined;

    constructor(
        private titleService: Title,
        private stayService: StayService,
        private patientService: PatientService,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private authenticationService: AuthenticationService,
        private employeeService: EmployeeService
    ) {}

    ngOnInit(): void {
        this.patientService.getById(this.getPatientId()).subscribe(
            patient => {
                this.patient = patient;
                this.setStays();
            },
            error => {
                this.alertService.error(error);
            }
        );

        const user = this.authenticationService.currentUserValue;
        this.employeeService.getById(user.ownerId as number, user.role as string).subscribe(
            data => this.employee = data
        );

        this.titleService.setTitle('Internări');
    }

    setStays(): void {
        this.stayService.getByPatientId(this.getPatientId()).subscribe(
            stays => {
                this.stays = stays;
            },
            error => {
                this.alertService.error(error);
            }
        );
    }

    deleteStay(id: number): void {
        this.stayService.delete(id).subscribe(
            () => this.setStays()
        );
    }

    getPatientId(): number {
        return this.route.snapshot.params.id;
    }

    goToStayPage(stayId: number): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/internari/${stayId}`]);
    }

    goToCreateStayPage(): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/internari/creare`]);
    }

    goToUpdateStayPage(stayId: number): void {
        this.router.navigate([`/pacienti/${this.getPatientId()}/internari/${stayId}/actualizare`]);
    }

    public isUserAdmin(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'ADMIN';
    }

    public isUserPatient(): boolean {
        const user = this.authenticationService.currentUserValue;
        return user.role === 'PATIENT';
    }

    public canUserUpdateStay(stayHospitalId: number): boolean {
        if (this.isUserPatient() || this.isUserAdmin()) {
            return false;
        }

        if (this.employee) {
            return this.employee.department.hospital.id === stayHospitalId;
        }

        return false;
    }
}
