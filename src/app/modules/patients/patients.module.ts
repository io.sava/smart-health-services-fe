import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsRoutingModule } from './patients-routing.module';
import { PatientsComponent } from './pages/patients/patients.component';
import { CreateComponent } from './pages/create/create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from 'src/app/core/material.module';
import { StaysComponent } from './pages/stays/stays.component';
import { AppointmentsComponent } from './pages/appointments/appointments.component';
import { StayComponent } from './pages/stay/stay.component';
import { CreateStayComponent } from './pages/create-stay/create-stay.component';
import { CreateUndergoComponent } from './pages/create-undergo/create-undergo.component';
import { UpdateStayComponent } from './pages/update-stay/update-stay.component';
import { AppointmentComponent } from './pages/appointment/appointment.component';
import { CreateAppointmentComponent } from './pages/create-appointment/create-appointment.component';
import { UpdateAppointmentComponent } from './pages/update-appointment/update-appointment.component';
import { CreatePrescriptionComponent } from './pages/create-prescription/create-prescription.component';


@NgModule({
  declarations: [
        PatientsComponent,
        CreateComponent,
        StaysComponent,
        AppointmentsComponent,
        StayComponent,
        CreateStayComponent,
        CreateUndergoComponent,
        UpdateStayComponent,
        AppointmentComponent,
        CreateAppointmentComponent,
        UpdateAppointmentComponent,
        CreatePrescriptionComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PatientsRoutingModule,
        CustomMaterialModule
    ]
})
export class PatientsModule { }
