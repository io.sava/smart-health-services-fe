import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { RoleGuard } from './core/guards/role.guard';
import { NotfoundComponent } from './shared/components/notfound/notfound.component';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'spitale',
        loadChildren: () => import('./modules/hospitals/hospitals.module').then(m => m.HospitalsModule)
    },
    {
        path: 'pacienti',
        loadChildren: () => import('./modules/patients/patients.module').then(m => m.PatientsModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'personal',
        loadChildren: () => import('./modules/employees/employees.module').then(m => m.EmployeesModule),
        canActivate: [AuthGuard, RoleGuard],
        data: {
            roles: ['PATIENT']
        }
    },
    {
        path: 'autentificare',
        loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
    },
    {
        path: 'deconectare',
        loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'profil',
        loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule),
        canActivate: [AuthGuard, RoleGuard],
        data: {
            roles: ['ADMIN']
        }
    },
    {
        path: '404',
        component: NotfoundComponent
    },
    {
        path: '**',
        redirectTo: '/404'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
