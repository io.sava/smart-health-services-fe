export const environment = {
  production: true,
  apiUrl: 'https://smart-health-services-monolith.herokuapp.com'
};
