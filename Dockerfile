FROM node:10-alpine as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build-prod

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=node /app/dist/smart-health-services-fe /usr/share/nginx/html
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/nginx.conf && nginx -g 'daemon off;'
